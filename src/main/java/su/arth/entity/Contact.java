package su.arth.entity;

import javax.persistence.*;

/**
 * Класс является сущностью Контакт
 * выполняет функцию адресной книги
 *
 * @param idContact ид контакта
 * @param nameContact имя контакта
 * @param owner  владелец контакта(в чьей адресной книге находится контакт)
 * @param contactUser  цель контакта
 */
@Entity
@Table(name = "contacts")
public class Contact {


    private Long idContact;
    private String nameContact;
    private User owner;
    private User contactUser;

    @Id
    @Column(name = "id_contact")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getIdContact() {
        return idContact;
    }

    public void setIdContact(Long idContact) {
        this.idContact = idContact;
    }

    @Basic
    @Column(name = "name_contact")
    public String getNameContact() {
        return nameContact;
    }

    public void setNameContact(String nameContact) {
        this.nameContact = nameContact;
    }

    @ManyToOne(fetch = FetchType.EAGER,cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REMOVE})
    @JoinColumn(name = "owner")
    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    @ManyToOne(fetch = FetchType.EAGER,cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REMOVE})
    @JoinColumn(name = "contact_user")
    public User getContactUser() {
        return contactUser;
    }

    public void setContactUser(User contactUser) {
        this.contactUser = contactUser;
    }
}
