package su.arth.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Класс является сущностью Пользователь
 * @param idUser ид пользователя
 * @param login логин
 * @param password  пароль
 * @param fio фио
 * @param email почта
 * @param roles роли
 * @param inputMessages входящие сообщения
 * @param outputMessages исходящие сообщения
 * @param contactList список контактов
 */
@Entity
@Table(name = "user")
public class User implements Serializable {
    private long idUser;
    private String login;
    private String password;
    private String fio;
    private String email;
    private Set<Role> roles = new HashSet<>();
    private List<Message> inputMessages = new ArrayList<>();
    private List<Message> outputMessages = new ArrayList<>();
    private List<Contact> contactList = new ArrayList<>();

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public User() {
    }

    @Id
    @Column(name = "id_user")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @JsonView(View.UI.class)
    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    @Basic
    @Column(name = "login")
//    @JsonView(View.UI.class)
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Basic
    @Column(name = "password")
 @JsonIgnore
//    @JsonView(View.REST.class)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "fio")
//    @JsonView(View.UI.class)
    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    @Basic
    @Column(name = "email")
//    @JsonView(View.UI.class)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }





    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
            joinColumns = { @JoinColumn(name = "user_id",referencedColumnName = "id_user") },
            inverseJoinColumns = { @JoinColumn(name = "role_id",referencedColumnName = "id_role") })
    public Set<Role> getRoles() {
        return this.roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }


    @OneToMany(mappedBy = "recipient",fetch = FetchType.EAGER,cascade = CascadeType.REMOVE)
    @JsonBackReference
    public List<Message> getInputMessages() {
        return inputMessages;
    }

    public void setInputMessages(List<Message> inputMessages) {
        this.inputMessages = inputMessages;
    }

    @OneToMany(mappedBy = "sender",fetch = FetchType.EAGER,cascade = CascadeType.REMOVE)
    @JsonBackReference
    public List<Message> getOutputMessages() {
        return outputMessages;
    }

    public void setOutputMessages(List<Message> outputMessages) {
        this.outputMessages = outputMessages;
    }


    @OneToMany(mappedBy = "owner",fetch = FetchType.EAGER)
    @JsonBackReference
    public List<Contact> getContactList() {
        return contactList;
    }

    public void setContactList(List<Contact> contactList) {
        this.contactList = contactList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (idUser != user.idUser) return false;
        if (login != null ? !login.equals(user.login) : user.login != null) return false;
        if (password != null ? !password.equals(user.password) : user.password != null) return false;
        if (fio != null ? !fio.equals(user.fio) : user.fio != null) return false;
        return email != null ? email.equals(user.email) : user.email == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (idUser ^ (idUser >>> 32));
        result = 31 * result + (login != null ? login.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (fio != null ? fio.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        return result;
    }
}
