package su.arth.dao;


import com.googlecode.genericdao.dao.jpa.GenericDAO;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

/** Интерфейс паттерна ДАО
 * наследует {@link GenericDAO} от googlecode.com
 * Является базовым ДАО интерфейсом
 *
 */
@Repository
public interface BaseDAO<T,PK extends Serializable> extends GenericDAO<T,PK> {

}
