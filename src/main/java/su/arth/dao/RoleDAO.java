package su.arth.dao;

import org.springframework.stereotype.Repository;
import su.arth.entity.Role;

/**
 * Интерфейс для управления ролями пользователей {@link Role}
 */
@Repository
public interface RoleDAO extends BaseDAO<Role,Long> {

    /**
     * Метод для получения из базы данных роли по типу роли
     * @param roleType тип роли
     * @return объект Role
     */
    Role findRole(String roleType);

}
