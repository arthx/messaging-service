package su.arth.dao;

import com.googlecode.genericdao.dao.jpa.GenericDAOImpl;
import com.googlecode.genericdao.search.jpa.JPASearchProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import su.arth.config.WebAppConfig;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;

/**
 * Данный класс реализует интерфейс {@link BaseDAO} наследуя класс {@link GenericDAOImpl} от googlecode.com
 *  В этом классе инжектится через setter метод EntityManager {@link WebAppConfig#entityManagerFactory()}
 *  и {@link WebAppConfig#jpaSearchProcessor()}
 */

@Repository
public class BaseDAOHibernateImpl<T,PK extends Serializable> extends GenericDAOImpl<T,PK> implements BaseDAO<T,PK> {


    @Override
    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        super.setEntityManager(entityManager);
    }

    @Override
    @Autowired
    public void setSearchProcessor(JPASearchProcessor searchProcessor) {
        super.setSearchProcessor(searchProcessor);
    }

}
