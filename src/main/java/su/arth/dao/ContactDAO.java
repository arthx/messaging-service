package su.arth.dao;

import org.springframework.stereotype.Repository;
import su.arth.entity.Contact;

import java.util.List;

/**
 * Интерфейс для {@link Contact}
 *
 */
@Repository
public interface ContactDAO extends BaseDAO<Contact,Long> {

    /**
     * Метод поиска всех контактов из базы данных
     * @param login логин пользователя чьи контакты необходимо получить
     * @return контакты пользователя
     */
    List<Contact> findContacts(String login);

}
