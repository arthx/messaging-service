package su.arth.dao;

import com.googlecode.genericdao.search.Search;
import org.springframework.stereotype.Repository;
import su.arth.entity.Role;

/**
 * Класс реализует интерфейс {@link RoleDAO}
 */
@Repository
public class RoleDAOHibernateImpl extends BaseDAOHibernateImpl<Role,Long> implements RoleDAO {

    @Override
    public Role findRole(String roleType) {
        Search search = new Search();
        search.addFilterEqual("roleType",roleType);

        return searchUnique(search);
    }

}
