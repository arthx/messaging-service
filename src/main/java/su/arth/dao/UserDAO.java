package su.arth.dao;

import org.springframework.stereotype.Repository;
import su.arth.entity.User;

import java.util.List;

/**
 * Интерфейс для упрвления сущностью {@link User}
 */
@Repository
public interface UserDAO extends BaseDAO<User,Long> {

    /**
     *  Поиск пользователя по логину
     * @param login логин пользователя
     * @return объект User
     */
    User findByLogin(String login);

    /**
     * Метод необходим для получения списка пользователей по  части логина
     * @param login логин пользователя
     * @return список пользователей удовлетворяющих параметру поиска
     */
    List<User> searchByLogin(String login);

    /**
     * Метод проверяет на существание пользователя в системе с таким логином
     * @param login логин пользователя
     * @return true или false по логину
     */
    boolean isLogin(String login);

}
