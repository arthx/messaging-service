package su.arth.dao;

import org.springframework.stereotype.Repository;
import su.arth.entity.Message;

import java.util.List;

/**
 * Интерфейс для управления сущностью {@link Message }
 *
 */
@Repository
public interface MessageDAO extends BaseDAO<Message,Long> {

   /**
    * Необходим для получения входящих сообщений
    * @param login логин пользователя
    * @return Входящие сообщения пользователя
     */
   List<Message> findInputUserMessage(String login);

   /**
    * Необходим для получения исходящих сообщений
    * @param login логин пользователя
    * @return Исходящие сообщения пользователя
    */
   List<Message> findOutputUserMessage(String login);
}
