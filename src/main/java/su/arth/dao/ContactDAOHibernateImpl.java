package su.arth.dao;

import org.springframework.stereotype.Repository;
import su.arth.entity.Contact;

import java.util.List;

/**
 * Класс реализует для Hibernate интерфейс {@link ContactDAO}
 *
 */
@Repository
public class ContactDAOHibernateImpl extends BaseDAOHibernateImpl<Contact,Long> implements ContactDAO {


    @Override
    public List<Contact> findContacts(String login) {
           return em().createQuery("select c from Contact c,User u WHERE c.owner.idUser = u.idUser and u.login =:login").setParameter("login",login).getResultList();
    }



}
