package su.arth.dao;

import org.springframework.stereotype.Repository;
import su.arth.entity.Message;

import java.util.List;

/**
 * Реализация для Hibernate интерфейса {@link MessageDAO}
 */
@Repository
public class MessageDAOHibernateImpl extends BaseDAOHibernateImpl<Message,Long> implements MessageDAO {
    /**
     *
     * @param login логин пользователя
     * @return
     */
    @Override
    public List<Message> findInputUserMessage(String login) {
      return  em().createQuery("select m from Message m,User u where m.recipient.idUser = u.idUser and u.login = :login").setParameter("login",login).getResultList();
    }

    /**
     *
     * @param login логин пользователя
     * @return
     */
    @Override
    public List<Message> findOutputUserMessage(String login) {
        return  em().createQuery("select m from Message m,User u where m.sender.idUser = u.idUser and u.login = :login").setParameter("login",login).getResultList();
    }


}
