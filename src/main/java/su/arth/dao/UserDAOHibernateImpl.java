package su.arth.dao;

import com.googlecode.genericdao.search.Search;
import org.springframework.stereotype.Repository;
import su.arth.entity.User;

import java.util.List;

/**
 * Данный класс реализует {@link UserDAO}
 *
 */
@Repository
public class UserDAOHibernateImpl extends BaseDAOHibernateImpl<User,Long> implements UserDAO {

    @Override
    public User findByLogin(String login) {

        Search search = new Search();
        search.addFilterEqual("login",login);
        return searchUnique(search);
         //entity manager query
        // return (User) em().createQuery("select u from User u where u.login = :login").setParameter("login",login).getResultList().get(0);


    }


    @Override
    public List<User> searchByLogin(String login) {
        login = "%"+login+"%";
     // как нибудь позже перепишу с использованием FullTextEntityManager fullText = org.hibernate.search.jpa.Search.getFullTextEntityManager(em());
      return em().createQuery("select u from User u where u.login like :login").setParameter("login",login).setMaxResults(12).getResultList();
    }

    @Override
    public boolean isLogin(String login) {
        Search search = new Search();
        // entitymanager query
        //  User login1 = (User) entityManager.createQuery("select u from User u where u.login = :login").setParameter("login", login).getResultList().get(0);
        User user = null;
        try {
            search.addFilterEqual("login",login);
            user = searchUnique(search);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return user!=null;
    }
}
