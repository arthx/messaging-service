package su.arth.config;

import com.googlecode.genericdao.search.jpa.JPAAnnotationMetadataUtil;
import com.googlecode.genericdao.search.jpa.JPASearchProcessor;
import org.apache.log4j.Logger;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.format.FormatterRegistry;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import su.arth.helpers.RoleConverter;

import javax.annotation.Resource;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;


/**
 * Created by arthur on 08.08.16.
 * Класс является конфигурацией для Spring
 * Использован тип конфигурации "Java Config"
 *  * @autor Salimkhanov Arthur
 * @version 1.0
 */

    @Configuration
    @EnableWebMvc
    @ComponentScan(basePackages = "su.arth")
    @EnableTransactionManagement
    @PropertySource("classpath:/db/db.properties")
    public class WebAppConfig extends WebMvcConfigurerAdapter {


   Logger logger = Logger.getLogger(WebAppConfig.class);
/**
*
* Инжектится конвертер RoleToUserConverter
 * для преобразования idRole с формы в объект Role
*
**/
      @Autowired
      private RoleConverter roleConverter;

    /*
     *  Данный конвертер дает ошибку с транзакциями при использовании merge закомментировал до выяснения и исправления причины
     *
      */
      /*@Autowired
      private ContactUserConverter contactUserConverter;
*/
    @Resource
    private Environment env;

    /**
     * Определение бина placeholderConfigurer
     *  служит для загрузки property
     *
     * */
        @Bean
        public static PropertySourcesPlaceholderConfigurer placeholderConfigurer(){
            return new PropertySourcesPlaceholderConfigurer();
        }

    /**
     * Метод addResourceHandler подключает статические ресурсты к проекту
     * */
         @Override
        public void addResourceHandlers(ResourceHandlerRegistry registry) {
            registry.addResourceHandler("/webjars/**").addResourceLocations("/webjars/");
            registry.addResourceHandler("/assets/**").addResourceLocations("/assets/");
        }

    /** Опеределение бина viewResolver
     *  служит для отображения view
     *  в данном проекте используется jsp
     *
     * */

        @Bean
        public ViewResolver getViewResolver(){
            InternalResourceViewResolver resolver = new InternalResourceViewResolver();
            resolver.setPrefix("/WEB-INF/pages/");
            resolver.setSuffix(".jsp");
            resolver.setViewClass(JstlView.class);
            return resolver;
        }


/** Определение бина entityManagerFactory
 *  Настраивается EntityManager:
 *  объявляется локация persistence.xml
 *  определяется какая библиотека реализует jpa
 *  в данном случае Hibernate
 *  @return em  возвращает настроенный EntityManager
* */
        @Bean
        @DependsOn(value = "flyway")
        public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
            LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();


           // em.setPersistenceUnitName("messaging-service-db");
           // em.setPersistenceXmlLocation("classpath:/config/db/persistence.xml");
           em.setDataSource(dataSource());
            em.setPackagesToScan(new String[] {"su.arth.entity"});

            JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
            em.setJpaVendorAdapter(vendorAdapter);
            em.setJpaProperties(additionalProperties());
            return em;
        }

    @Bean
    public DataSource dataSource(){
        String driverClassName = env.getProperty("jdbc.driverClassName");
        String url = env.getProperty("jdbc.url");
        String user = env.getProperty("jdbc.username");
        String password = env.getProperty("jdbc.password");
        //  int minPoolSize = Integer.parseInt(env.getProperty("jdbc.minPoolSize"));
       // int maxPoolSize = Integer.parseInt(env.getProperty("jdbc.maxPoolSize"));
       // int maxStatements = Integer.parseInt(env.getProperty("jdbc.maxStatements"));

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(driverClassName);
        dataSource.setUrl(url);
        dataSource.setUsername(user);
        dataSource.setPassword(password);
        return dataSource;
    }


    /** Бин определяющий работу с транзакциями
     * на входе EntityManagerFactory
     * @param entityManagerFactory
    * */

        @Bean
        public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory){
            JpaTransactionManager transactionManager = new JpaTransactionManager();
            transactionManager.setEntityManagerFactory(entityManagerFactory);
            return transactionManager;
        }

        @Bean
        public PersistenceExceptionTranslationPostProcessor exceptionTranslation(){
            return new PersistenceExceptionTranslationPostProcessor();
        }

      /** Данный бин определяется для более удобной манипуляции данными из бд
       *
       *
       * */
        @Bean
        public JPASearchProcessor jpaSearchProcessor(){
            JPASearchProcessor jpaSearchProcessor = new JPASearchProcessor(new JPAAnnotationMetadataUtil());
            return jpaSearchProcessor;
        }


    /**
     * Добавление ковертера {@link RoleConverter}
     * @param registry
     */
    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(roleConverter);
//        registry.addConverter(contactUserConverter);
    }

    Properties additionalProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto", "update");
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLInnoDBDialect");
        properties.setProperty("hibernate.show_sql","true");
        properties.setProperty("hibernate.format_sql","true");
        return properties;
    }


    @Bean(initMethod = "migrate")
    Flyway flyway() {
        Flyway flyway = new Flyway();
        flyway.setBaselineOnMigrate(true);
        flyway.setLocations("/db/migration");
        flyway.setDataSource(dataSource());
       return flyway;
    }



}

