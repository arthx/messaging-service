package su.arth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.ExceptionMappingAuthenticationFailureHandler;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.filter.CharacterEncodingFilter;

import java.util.HashMap;
import java.util.Map;

/**
 * Данный класс является конфигурацией Spring Security
 *
 */

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    /**
     * Инжектит сервис {@link su.arth.service.MessagingUserDetailsService}
     */
    @Autowired
    @Qualifier("userDetailsService")
    UserDetailsService userDetailsService;


    /**
     *  Определение AuthentificationProvider
     * @param auth
     * @throws Exception
     */
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider());
    }


    /**
     *  В методе настраивается доступ групп пользователей к различным частям приложения
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        CharacterEncodingFilter filter = new CharacterEncodingFilter();
        filter.setEncoding("UTF-8");
        filter.setForceEncoding(true);
        http.addFilterBefore(filter,CsrfFilter.class);
        // открываем доступ к статическим файлам приложения
        http.authorizeRequests()
                .antMatchers("/assets/**","/webjars/**")
                .permitAll().and();

        //открываем доступ к путям /user /error для обеспечения ввода регистрационных данных
        http.authorizeRequests().antMatchers("/user","/error/**").permitAll().and();

        // Закрываем остальную часть приложения для не аутентифицированных пользователей
        http.authorizeRequests().antMatchers("/**").access("isAuthenticated()").and();

        // Доступ только для группы ROLE_ADMIN
        http.authorizeRequests().antMatchers("/admin")
                .access("hasRole('ROLE_ADMIN')").and();

       // указание для формы авторизации
        http.formLogin()
                .loginPage("/signin")
                .loginProcessingUrl("/j_spring_security_check")
                .failureHandler(authenticationFailureHandler())
                .usernameParameter("login")
                .passwordParameter("password")
                .permitAll().and();

        http.logout()
                .logoutUrl("/logout")
                .permitAll()
                .invalidateHttpSession(true).and().csrf()
                .and();

        http.sessionManagement().maximumSessions(1);


    }


    /**
     *  Определение бина, который создает {@link BCryptPasswordEncoder}
     *  для шифрования пароля пользователей
     * @return encoder
     */
    @Bean
    public PasswordEncoder passwordEncoder(){
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder;
    }


    /** В данном бине
     * создается DaoAuthenticationProvider который необходим для получения логина и пароля пользователя
     * в нем определяется метод шифрующий пароль
     * а так же сервис для управления авторизацией {@link su.arth.service.MessagingUserDetailsService}
     * @return
     */
    @Bean
    public DaoAuthenticationProvider authProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    /** Создается бин, который мапит стандартные эксепшены при авторизации пользователя на пути для обработки контроллером
     *
     * @return
     */
    @Bean
    ExceptionMappingAuthenticationFailureHandler authenticationFailureHandler(){
        ExceptionMappingAuthenticationFailureHandler ex = new ExceptionMappingAuthenticationFailureHandler();
        Map<String, String> mappings = new HashMap<String, String>();
        mappings.put("org.springframework.security.authentication.CredentialsExpiredException", "/error/reset");
        mappings.put("org.springframework.security.authentication.LockedException", "/error/locked");
        mappings.put("org.springframework.security.authentication.BadCredentialsException", "/error/creditials");
        mappings.put("org.springframework.security.core.userdetails.UsernameNotFoundException", "/error/usernf");
        mappings.put("org.springframework.security.authentication.InternalAuthenticationServiceException","/error/usernf");

        ex.setExceptionMappings(mappings);
        return ex;
    }
}

