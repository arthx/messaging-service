package su.arth.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Класс необходим для определения Spring Security
 * реализует {@link AbstractSecurityWebApplicationInitializer}
 *
 */
public class SecurityInit extends AbstractSecurityWebApplicationInitializer {
}
