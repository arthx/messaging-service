package su.arth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.*;
import su.arth.entity.Role;
import su.arth.entity.User;
import su.arth.helpers.MainHelper;
import su.arth.helpers.RoleHelper;
import su.arth.helpers.RoleType;
import su.arth.service.RoleService;
import su.arth.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

/**
 * Данный класс помечается аннотацией Controller
 * является контроллером в Spring MVC
 * мапит путь /admin
 * доступен только для группы ROLE_ADMIN
 *
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

    /**
     * Инжектится сервис {@link UserService}
     */
    @Autowired
    UserService userService;

    /**
     * Инжектится сервис {@link RoleService}
     */
    @Autowired
    RoleService roleService;


    /**
     *  Метод управления пользователями
     *  возможно удаление пользователей, изменение ролей
     * @param request
     * @param model
     * @param auth
     * @return "admin/users"
     */
   @RequestMapping(value = "/users",method = RequestMethod.GET)
    public String getUsers(HttpServletRequest request, Model model,Authentication auth){
       String login = MainHelper.getLogin(auth);
       PagedListHolder<User> pagedListHolder = new PagedListHolder<User>(userService.listUserWithoutMe(login));
       int page = ServletRequestUtils.getIntParameter(request, "p", 0);
       pagedListHolder.setPage(page);
       pagedListHolder.setPageSize(10);
       model.addAttribute("pagedListHolder", pagedListHolder);
       return "admin/users";
   }


    /** /change/{id}
     * Изменение роли пользователя:
     *   получени информации о вхождении пользователя в группу ROLE_ADMIN
     * @param idUser Ид пользователя у которого изменяется роль
     * @return roleHelper возврат объекта {@link RoleHelper} с преобразованием в json-формат
     */
    @RequestMapping(value = "/change/{id}",method = RequestMethod.GET)
    public @ResponseBody
    RoleHelper changeUser(@PathVariable(value = "id") Long idUser){
        User user = userService.findUserById(idUser);
        RoleHelper roleHelper = new RoleHelper();
        boolean match = user.getRoles().stream().anyMatch(role -> role.equals(roleService.findRole(RoleType.ROLE_ADMIN.getRoleType())));
        roleHelper.setRoleAdmin(String.valueOf(match));
        roleHelper.setIdUser(user.getLogin());
        return roleHelper;
    }


    /** /change
     *  В методе выполняется добавление или удаление  роли ROLE_ADMIN пользователю
     * @param roleHelper
     * @return возврат json-строки status: ok если изменение роли прошло успешно, status: error при возниконовении ошибки
     *
     */
    @RequestMapping(value = "/change",method = RequestMethod.POST)
    @ResponseBody
    public String saveUser(@RequestBody RoleHelper roleHelper){
       try {
           User user = userService.findUserById(Long.parseLong(roleHelper.getIdUser()));
           Set<Role> roles = user.getRoles();

           if (roleHelper.getRoleAdmin().contains("true")) {
               roles.add(roleService.findRole(RoleType.ROLE_ADMIN.getRoleType()));
               userService.updateUser(user);
           } else {
               roles.remove(roleService.findRole(RoleType.ROLE_ADMIN.getRoleType()));
               userService.updateUser(user);
           }
       } catch (Exception e){
           return "{\"status\":\"error\"}";
       }
        return "{\"status\":\"OK\"}";
    }


}
