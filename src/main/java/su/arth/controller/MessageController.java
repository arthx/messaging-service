package su.arth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import su.arth.entity.Message;
import su.arth.helpers.MainHelper;
import su.arth.helpers.MessageHelper;
import su.arth.service.MessageService;
import su.arth.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Класс является контроллером Spring MVC
 *
 */
@Controller
@RequestMapping("/message")
public class MessageController {

    /**
     *  Инжектится {@link MessageService }
     */
    @Autowired
    MessageService messageService;
    /**
     *  Инжектится {@link UserService }
     */
    @Autowired
    private UserService userService;


    /**
     *  Принимает данные и сохраняет их в виде объекта {@link Message}
     *  в случае ошибки возвращает status:danger, message:Ошибка отправки
     * @param auth
     * @param messageHelper
     * @param model
     * @return  redirect /
     */
    @RequestMapping(value = "/new",method = RequestMethod.POST)
    public String sendMessage(Authentication auth, MessageHelper messageHelper,Model model){

        try {
       //тут пока будет как то так, т.к. с converter непонятная ошибка с транзакцией
            Message message = new Message();
            message.setDate(new Timestamp(Calendar.getInstance().getTimeInMillis()));
            message.setRecipient(userService.findUserById(Long.parseLong(messageHelper.getRecipient())));
            message.setSender(userService.findUser(MainHelper.getLogin(auth)));
            message.setContentMessage(messageHelper.getContent_message());
            message.setTheme(messageHelper.getTheme());
            messageService.save(message);
        } catch (Exception e){
           model.addAttribute("status","danger");
            model.addAttribute("message","Ошибка отправки");
        }
        return "redirect:/";
    }


    /** /message/output
     *  Данный метод отвечает за получение пользователем исходящих сообщений
     * @param model
     * @param auth
     * @param request
     * @return
     */
    @RequestMapping("/output")
    public String getOutputMessage(Model model, Authentication auth, HttpServletRequest request){
        String login = MainHelper.getLogin(auth);
        PagedListHolder<Message> outputMessageList = new PagedListHolder<Message>(messageService.findOutputUserMessages(login));
        int page = ServletRequestUtils.getIntParameter(request, "p", 0);
        outputMessageList.setPage(page);
        outputMessageList.setPageSize(12);
        model.addAttribute("outputMessageList", outputMessageList);
        return "index";
    }



    /** /message/input
     *  Данный метод отвечает за получение пользователем входящих сообщений
     * @param model
     * @param auth
     * @param request
     * @return
     */
    @RequestMapping("/input")
    public String getInputMessage(Model model, Authentication auth, HttpServletRequest request){
        String login = MainHelper.getLogin(auth);
        PagedListHolder<Message> inputMessageList = new PagedListHolder<Message>(messageService.findInputUserMessages(login));
        int page = ServletRequestUtils.getIntParameter(request, "p", 0);
        inputMessageList.setPage(page);
        inputMessageList.setPageSize(12);
        model.addAttribute("inputMessageList", inputMessageList);
        return "index";
    }

    /**
     * Метод возвращает объект {@link Message} сконвертированный в json формат,
     * принимает id сообщения выбранного из таблицы сообщений
     * @param idMessage
     * @return
     */
    @RequestMapping("/{id}")
    public @ResponseBody Message getMessage(@PathVariable(value = "id") Long idMessage){
        return messageService.find(idMessage);
    }



    @RequestMapping("/all")
    @Secured(value = "hasRole('ROLE_ADMIN')")
    public String getAllMessage(Model model, Authentication auth, HttpServletRequest request){
        String login = MainHelper.getLogin(auth);
        PagedListHolder<Message> allMessageList = new PagedListHolder<Message>(messageService.findAllMessages());
        int page = ServletRequestUtils.getIntParameter(request, "p", 0);
        allMessageList.setPage(page);
        allMessageList.setPageSize(12);
        model.addAttribute("allMessageList", allMessageList);
        return "index";
    }


}
