package su.arth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import su.arth.entity.Contact;
import su.arth.helpers.ContactHelper;
import su.arth.helpers.MainHelper;
import su.arth.service.ContactService;
import su.arth.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/** Класс является контроллером  Spring
 * мапит путь /contacts
 * Отвечает за обработку Контактов ({@link Contact}) пользователей
 *
 */
@Controller
@RequestMapping("/contacts")
public class ContactController {

    /**
     *  Инжектит сервис  {@link UserService}
     */
    @Autowired
    private UserService userService;

    /**
     *  Инжектит сервис  {@link ContactService}
     */
    @Autowired
    private ContactService contactService;

    /**
     * Метод возвращает все существующие контакты пользователя
     * @param request
     * @param model
     * @param auth
     * @return
     */
 @RequestMapping(method = RequestMethod.GET)
 public String index(HttpServletRequest request, Model model, Authentication auth){

     String login = MainHelper.getLogin(auth);

     PagedListHolder<Contact> contacts = new PagedListHolder<Contact>(contactService.findContacts(login));
     int pageInput = ServletRequestUtils.getIntParameter(request, "p", 0);
     contacts.setPage(pageInput);
     contacts.setPageSize(12);
     model.addAttribute("contacts", contacts);

     return "user/contact";
 }

    /**
     * Метод необходим для получения контактов пользователя в формате  json
     * @param auth
     * @return contacts
     */
    @RequestMapping(value = "/my",method = RequestMethod.GET)
    public @ResponseBody List<Contact> findContact(Authentication auth){
        String login = MainHelper.getLogin(auth);
        List<Contact> contacts = contactService.findContacts(login);

        return contacts;
    }


    /**
     *  Создание нового контакта
     *  используется   {@link ContactHelper}  для конвертации из json в Java объект
     *  добавляется в модель
     *  если успешно:
     *  status : success
     *  message: Контакт успешно добавлен
     *   если возникла ошибка:
     *   status: danger
     *   message: не удалось добавить контакт
     * @param auth
     * @param contactHelper
     * @param request
     * @param model
     * @return contacts Возвращение обновленного списка контактов
     *
     *
     *
     */
    @RequestMapping(value = "/new",method = RequestMethod.POST)
    public String addContact(Authentication auth, ContactHelper contactHelper,HttpServletRequest request,Model model){
   try {
       Contact contact = new Contact();
       contact.setNameContact(contactHelper.getNameContact());
       contact.setOwner(userService.findUser(MainHelper.getLogin(auth)));
       contact.setContactUser(userService.findUserById(contactHelper.getContactUser()));
       contactService.save(contact);
       model.addAttribute("status","success");
       model.addAttribute("message","Контакт успешно добавлен");
   } catch (Exception e){
       model.addAttribute("status","danger");
       model.addAttribute("message","Не удалось добавить контакт");
   }
        PagedListHolder<Contact> contacts = new PagedListHolder<Contact>(contactService.findContacts(MainHelper.getLogin(auth)));
        int pageInput = ServletRequestUtils.getIntParameter(request, "p", 0);
        contacts.setPage(pageInput);
        contacts.setPageSize(5);
        model.addAttribute("contacts",contacts);
        return "user/contact";
    }

    @RequestMapping("/remove/{id}")
    public String removeContact(@PathVariable(value = "id") Long id){
      contactService.removeContact(id);
        return "redirect:/contacts";
    }


}
