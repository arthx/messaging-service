package su.arth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import su.arth.entity.User;
import su.arth.helpers.MainHelper;
import su.arth.helpers.Response;
import su.arth.helpers.UserJsonResponse;
import su.arth.service.UserService;

import java.util.ArrayList;
import java.util.List;

/**
 * Контроллер Spring MVC отвечает за работу с объектами пользователя {@link User}
 *
 */
@Controller
@RequestMapping("/user")
public class UserController {

    /**
     * Инжектит сервис {@link UserService}
     */
    @Autowired
    private UserService userService;
    /**
     * Инжектит бин {@link PasswordEncoder}
     */
    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * Данный метод создает в модели объект {@link User}
     *
     * @param model
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public String createUser(Model model){
        model.addAttribute("user", new User());
        return "user/edit";
    }

    /**
     * Метод сохраняет данные полученные с View
     * проверяет на наличие логина в базе данных, и ошибок валидации
     *
     * @param user
     * @param bindingResult
     * @param model
     * @return redirect: /admin/users если добавлен был пользователь с панели админа
     *  redirect: / если пользователь зарегистрировался
     */
    @RequestMapping(method = RequestMethod.POST)
    public String addUser(User user, BindingResult bindingResult, Model model, Authentication auth){
        if(bindingResult.hasErrors()){
            model.addAttribute("message","Введите корректные данные");
            model.addAttribute("status","info");
            return "user/edit";
        }

        if (userService.isLogin(user.getLogin())) {
            model.addAttribute("message", "Пользователь с данным логин уже зарегистрирован");
            model.addAttribute("status", "danger");

            return "redirect:/signin";
        }

        userService.addUser(user);

        if (MainHelper.getLogin(auth).isEmpty()){
            model.addAttribute("status", "success");
            model.addAttribute("message", "Добро пожаловать");
            return "redirect:/";
        }

        return "redirect:/admin/users";
    }


    /**
     *  Метод отображающий личную страницу пользователя на которой можно изменить пароль
     * @param model
     * @param auth
     * @return
     */
    @RequestMapping("/mypage")
    public String showMyPage(Model model, Authentication auth){
        String login = MainHelper.getLogin(auth);
        User user = userService.findUser(login);
        model.addAttribute("user",user);
        return "user/mypage";
    }

    /**
     * Изменение пользователя по его ид
     * Метод доступен только для группы ROLE_ADMIN
     * @param idUser
     * @param model
     * @return
     */
    @RequestMapping(value = "/change/{id}")
    @Secured(value = "hasRole('ROLE_ADMIN')")
    public String changeUser(@PathVariable(value = "id") Long idUser,Model model){
       model.addAttribute("user",userService.findUserById(idUser));
        return "/user/edit";
    }

   /* @InitBinder
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws     Exception {
        binder.registerCustomEditor(Role.class, "roles", new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                Role role = roleService.findById(Long.parseLong(text));
                setValue(role);
            }
        });
    }*/

    /**
     * Метод для удаления пользователя
     * доступен только для пользователей группы ROLE_ADMIN
     * @param id
     * @return
     */
    @Secured(value = "hasRole('ROLE_ADMIN')")
     @RequestMapping(value = "/remove/{id}")
     public String remove(@PathVariable(value = "id") Long id){
         userService.remove(id);
         return "redirect:/admin/users";
     }


    /**
     * Метод для получения всех пользователей
     * по полю search
     *
     * @param auth {@link Authentication}
     * @param search параметр признак поиска по логину
     * @return
     */
    @RequestMapping(value = "/search")
    public @ResponseBody List<UserJsonResponse> searchUsers(Authentication auth, @RequestParam(value = "search",required = false) String search){

        List<UserJsonResponse> usersJson = new ArrayList<UserJsonResponse>();
     List<User> users = userService.searchUser(MainHelper.getLogin(auth),search);
        for (User user : users){
            UserJsonResponse userJsonResponse = new UserJsonResponse();
               userJsonResponse.setId(String.valueOf(user.getIdUser()));
               userJsonResponse.setText(user.getLogin());
            usersJson.add(userJsonResponse);
        }

        return usersJson;
    }


    /**
     * Метод для обновления пароля
     * принимает старый пароль и новый пароль, сверяет старый пароль с тем что хранится в базе данных
     * если пароли совпадают, то обновляет старый пароль
     * @param auth
     * @param password действующий пароль
     * @param newPassword новый пароль
     * @return Response {@link Response}
     * При успешном изменении пароля
     *  status: "success"
     *  message: "Ваш пароль успешно изменен"
     *  При не совпадении введеного пароля и пароля из базы данных
     *  status: "danger"
     *  message: "Введите правильный действуюший пароль"
     *  При другой ошибке:
     *  status: "danger"
     *  message: "Произошла ошибка при изменении пароля"
     *
     */
    @RequestMapping("/password/new")
    public @ResponseBody
    Response changePassword(Authentication auth, @RequestParam(value = "password") String password, @RequestParam(value = "newPassword") String newPassword){
        Response response = new Response();
        User user = userService.findUser(MainHelper.getLogin(auth));
        if(passwordEncoder.matches(password,user.getPassword())){
            try {
                userService.changeUserPassword(user,newPassword);
                response.setStatus("success");
                response.setMessage("Ваш пароль успешно изменен");
              } catch (Exception e){
                response.setStatus("danger");
                response.setMessage("Произошла ошибка при изменении пароля");
            }
        } else {
            response.setStatus("danger");
            response.setMessage("Введите правильный действующий пароль");
        }
        return response;
    }
}
