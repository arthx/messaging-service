package su.arth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import su.arth.config.SecurityConfig;
import su.arth.entity.Message;
import su.arth.helpers.MainHelper;
import su.arth.service.MessageService;
import su.arth.service.RoleService;
import su.arth.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Контроллер Spring MVC
 * мапит корневой путь /
 * Отвечает за главную страницу, на которой расположены входящие и исходящие сообщения
 *
 */
@Controller
@RequestMapping("/")
public class MainController {

    /**
     * Инжектит сервис {@link MessageService}
     */
    @Autowired
    MessageService messageService;

    @Autowired
    RoleService roleService;

    /**
     * Инжектит сервис {@link UserService}
     */
    @Autowired
    UserService userService;

    /** / корневой путь
     *   Метод добавляет в модель объекты {@link PagedListHolder} с входящими и исходящими сообщениями
     *   необходим для пагинации
     * @param request получение из данного параметра номера страницы пагинации
     * @param model модель
     * @param auth получение логина из данного параметра
     * @return "index"
     */
    @RequestMapping(method = RequestMethod.GET)
    public String index(HttpServletRequest request,Model model,Authentication auth){

       String login = MainHelper.getLogin(auth);


        PagedListHolder<Message> inputMessageList = new PagedListHolder<Message>(messageService.findInputUserMessages(login));
        int pageInput = ServletRequestUtils.getIntParameter(request, "p", 0);
        inputMessageList.setPage(pageInput);
        inputMessageList.setPageSize(12);

        PagedListHolder<Message> outputMessageList = new PagedListHolder<Message>(messageService.findOutputUserMessages(login));
        int page = ServletRequestUtils.getIntParameter(request, "p", 0);
        outputMessageList.setPage(page);
        outputMessageList.setPageSize(12);
        model.addAttribute("outputMessageList", outputMessageList);
        model.addAttribute("inputMessageList", inputMessageList);

 if (auth!=null && auth.getAuthorities().stream().anyMatch(role -> role.getAuthority().contains("ROLE_ADMIN"))){
     PagedListHolder<Message> allMessageList = new PagedListHolder<Message>(messageService.findAllMessages());
     int pageAll = ServletRequestUtils.getIntParameter(request, "p", 0);
     allMessageList.setPage(pageAll);
     allMessageList.setPageSize(12);
     model.addAttribute("allMessageList", allMessageList);
 }

        return "index";
    }


    /**
     * Метод для авторизации Spring Security
     * @return
     */
    @RequestMapping(value = "/signin",method = RequestMethod.GET)
    public String signin(){
             return "signin";
    }


    /**
     * Метод для логаута Spring Security
     * @return
     */

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/signin?logout";
    }


    /**
     * Метод отображения ошибок при авторизации {@link SecurityConfig#authenticationFailureHandler()}
     * @param error
     * @param model
     * @return
     */
     @RequestMapping(value="/error/{type}",method = RequestMethod.GET)
     public String error(@PathVariable(value = "type") String error,Model model){

         if(error!=null){
             switch (error){
                 case "locked":
                     model.addAttribute("msg","Логин занят");
                     model.addAttribute("css","danger");
                     break;
                 case "usernf":
                     model.addAttribute("msg","Пользователя с данным логин не существует");
                     model.addAttribute("css","danger");
                     break;
                 case "creditials":
                     model.addAttribute("msg","Неверный логин или пароль");
                     model.addAttribute("css","danger");
                     break;
                 case "reset":
                     model.addAttribute("msg","Ошибка при входе. попробуйте еще раз");
                     model.addAttribute("css","danger");
                     break;
                 default:
                     model.addAttribute("msg","Ошибка при входе. попробуйте еще раз");
                     model.addAttribute("css","danger");
             }
         }

         return "signin";
     }


    @RequestMapping(name="/signin",method = RequestMethod.GET, params = "logout")
    public String logoutSingin(Model model){
        model.addAttribute("logout","Сеанс завершен.");


        return "redirect:/signin";
    }



}
