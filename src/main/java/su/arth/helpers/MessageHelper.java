package su.arth.helpers;

/**
 * Класс необходим для преобразования из json строки в объект java
 */
public class MessageHelper {
   private String recipient;
    private String theme;
    private String content_message;

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getContent_message() {
        return content_message;
    }

    public void setContent_message(String content_message) {
        this.content_message = content_message;
    }
}
