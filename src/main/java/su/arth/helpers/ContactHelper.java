package su.arth.helpers;

/**
 * Класс необходим для преобразования из JSON строки в объект java
 * @param nameContact имя контакта
 * @param contactUser ид объекта пользователь который должен быть добавлен в контакт
 */
public class ContactHelper {

    private String nameContact;
    private Long contactUser;

    public String getNameContact() {
        return nameContact;
    }

    public void setNameContact(String nameContact) {
        this.nameContact = nameContact;
    }

    public Long getContactUser() {
        return contactUser;
    }

    public void setContactUser(Long contactUser) {
        this.contactUser = contactUser;
    }
}
