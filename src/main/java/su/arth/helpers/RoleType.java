package su.arth.helpers;

/**
 * Данный enum необходим для удобного управления ролями пользователей
 */
public enum RoleType {


    ROLE_USER("ROLE_USER","Пользователь"),
    ROLE_ADMIN("ROLE_ADMIN","Администратор");


    String roleType;
    String roleName;

    private RoleType(String roleType, String roleName){
        this.roleType = roleType;
        this.roleName = roleName;
    }

    public String getRoleType(){
        return roleType;
    }

    public String getRoleName() {
        return roleName;
    }


}