package su.arth.helpers;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Данный класс помощник
 * в котором основные методы, использующиеся во многих других классах
 */
public class MainHelper {

    /**
     * Метод получения логина авторизованного пользователя из {@link Authentication}
     * @param auth
     * @return логин авторизованного пользователя
     * возвращает пустую строку в случае если auth==null
     */
    public static String getLogin(Authentication auth){
        String login = "";
        try {
            Object principal = auth.getPrincipal();
            if (principal instanceof UserDetails) {
                login = ((UserDetails) principal).getUsername();
            } else {
                login = principal.toString();
            }
        } catch (Exception e){
            //logger
        }
        return login;
    }
}
