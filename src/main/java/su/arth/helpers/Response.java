package su.arth.helpers;

import java.util.List;

/**
 * Класс который содержит статус и сообщение об успешности выполнения операций
 * может содержать дополнительный список данных которые должны вернуться во view
 */
public class Response {


  private String status;
   private String message;
 private List items;

    /**
     * получить список
     * @return
     */
    public List getItems() {
        return items;
    }

    public void setItems(List items) {
        this.items = items;
    }

    /**
     * получить статус
     * @return
     */
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * получить сообщение
     * @return
     */
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
