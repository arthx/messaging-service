package su.arth.helpers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import su.arth.entity.Role;
import su.arth.service.RoleService;

/**
 * Класс конвертирует ид роли в объект роль из базы данных
 */
@Component
public class RoleConverter implements Converter<Object, Role> {

        @Autowired
        RoleService roleService;


    public Role convert(Object element) {
        Long id = Long.parseLong((String)element);
        Role role= roleService.findById(id);
        return role;
    }

}
