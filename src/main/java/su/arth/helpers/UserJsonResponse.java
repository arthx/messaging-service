package su.arth.helpers;

/**
 * Данный класс возвращает в select ид и текст(логин пользователя) для отображения в форме создания контактов
 */

public class UserJsonResponse {

    private String id;
    private String text;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
