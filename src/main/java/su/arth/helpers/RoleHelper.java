package su.arth.helpers;

/**
 * Created by arthur on 11.08.16.
 */
public class RoleHelper {

    private String idUser;
    private String roleAdmin;

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getRoleAdmin() {
        return roleAdmin;
    }

    public void setRoleAdmin(String roleAdmin) {
        this.roleAdmin = roleAdmin;
    }
}
