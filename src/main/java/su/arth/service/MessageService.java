package su.arth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import su.arth.dao.MessageDAO;
import su.arth.entity.Message;
import su.arth.entity.User;

import java.util.List;

/**
 * Класс является сервисом Spring
 * ведется управление данными (Сообщения) из базы данных
 *
 */
@Service
public class MessageService {


    /**
     * Инжектится {@link MessageDAO}
     */
  @Autowired
  MessageDAO messageDAO;

    /**
     * Инжектится {@link UserService}
     */
@Autowired
 UserService userService;


    /**
     *  выборка всех сообщенийиз базы данных
     * @return список всех сообщений
     */
    public List<Message> findAllMessages() {
        return messageDAO.findAll();
    }

    /**
     * выборка входящих сообщений пользователя из базы данных
     * @param login логин пользователя
     * @return список входящих сообщений пользователя
     */
    public List<Message> findInputUserMessages(String login) {
       User user = userService.findUser(login);
     return  user.getInputMessages();
 }

    /**
     * Выборка исходящих сообщений пользователя
     * @param login - логин пользователя
     * @return список исходящих сообщений пользователя
     */
    public List<Message> findOutputUserMessages(String login) {
        User user = userService.findUser(login);
        return user.getOutputMessages();
   }

    /**
     * выборка сообщения по ид
     * @param id ид сообщения
     * @return сообщение
     */
    public Message find(Long id){
       return messageDAO.find(id);
    }


    /**
     * сохранение сообщения
     * @param message сообщение для сохранения в базе данных
     */
    @Transactional
    public void save(Message message){
        messageDAO.merge(message);
    }

}
