package su.arth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import su.arth.dao.UserDAO;
import su.arth.entity.Role;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Класс является сервисом Spring
 * отвечает за авторизацию пользователей
 *
 */
@Service(value = "userDetailsService")
public class MessagingUserDetailsService implements UserDetailsService {

    /**
     * Инжектится {@link UserDAO}
     */
    @Autowired
    private UserDAO userDAO;


    /**
     * Загружает пользователя по логину
     * наделяет ролями
     * и передает на создание {@link User} UserDetails
     * @param login
     * @return
     * @throws UsernameNotFoundException
     */
    @Transactional(readOnly=true)
    @Override
    public UserDetails loadUserByUsername(final String login)
            throws UsernameNotFoundException {

        su.arth.entity.User user = userDAO.findByLogin(login);
        List<GrantedAuthority> authorities =
                buildUserAuthority(user.getRoles());

        return buildUserForAuthentication(user, authorities);

    }

    /**
     * Создает объект {@link User}
     * @param user объект пользователя
     * @param authorities список групп
     * @return
     */
    private User buildUserForAuthentication(su.arth.entity.User user,
                                            List<GrantedAuthority> authorities) {
        return new User(user.getLogin(), user.getPassword(), true, true, true, true, authorities);
    }

    /**
     * формирует список групп пользователей
     * @param roles
     * @return
     */
    private List<GrantedAuthority> buildUserAuthority(Set<Role> roles) {
        Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();
        for (Role role : roles) {
            setAuths.add(new SimpleGrantedAuthority(role.getRoleType()));
        }

        List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(setAuths);
        return Result;
    }

}
