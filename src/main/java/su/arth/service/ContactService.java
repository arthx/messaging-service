package su.arth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import su.arth.dao.ContactDAO;
import su.arth.entity.Contact;

import java.util.List;

/**
 * Класс является сервисом Spring
 * здесь осуществляется манипуляция данными контактов из базы данных
 *  */
@Service
public class ContactService {


    /**
     * Инжектится {@link ContactDAO}
     */
     @Autowired
     private ContactDAO contactDAO;

    /**
     * Инжектится {@link UserService}
     */
    @Autowired
    UserService userService;

    /**
     * поиск в базе контактов по логину пользователя
     * @param login логин пользователя
     * @return список контактов
     */
    @Transactional
    public List<Contact> findContacts(String login){
     return contactDAO.findContacts(login);
    }


    /**
     * сохранение контакта
     * @param contact объект контакт для сохранения
     */
    @Transactional
    public void save(Contact contact){
        contactDAO.merge(contact);
    }


    public Contact findContactById(Long id){
       return contactDAO.find(id);
    }


    @Transactional
    public void removeContact(Long id){
                contactDAO.remove(findContactById(id));
    }

}
