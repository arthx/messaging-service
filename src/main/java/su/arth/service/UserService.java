package su.arth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import su.arth.dao.UserDAO;
import su.arth.entity.Contact;
import su.arth.entity.Role;
import su.arth.entity.User;
import su.arth.helpers.RoleType;

import java.util.List;
import java.util.Set;

/**
 * Данный класс является сервисом Spring
 * Выполняет управление данными с сущностью User
 */
@Service
public class UserService {

    /**
     * Инжектит {@link UserDAO}
     */
    @Autowired
    private UserDAO userDAO;

    /**
     * Инжектит {@link RoleService}
     */
    @Autowired
    private RoleService roleService;

    /**
     * Инжектит {@link PasswordEncoder}
     */
    @Autowired
    PasswordEncoder passwordEncoder;

    /**
     * Добавляет нового пользователя {@link User}
     * @param user пользователь
     */
    @Transactional
    public void addUser(User user){
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Set<Role> roles = user.getRoles();
        Role role = roleService.findRole(RoleType.ROLE_USER.getRoleType());
        roles.add(role);
        userDAO.merge(user);
    }

    /**
     * Обновляет пароль пользователя
     * @param user пользователь
     * @param newPassword новый пароль
     */
    @Transactional
    public void changeUserPassword(User user,String newPassword){
        String encodePassword = passwordEncoder.encode(newPassword);
        user.setPassword(encodePassword);
        userDAO.merge(user);
    }

    /**
     * выборка списка пользователей без данного пользователя по логину
     * @param login
     * @return
     */
    public List<User> listUserWithoutMe(String login){
        User user = userDAO.findByLogin(login);
        List<User> all = userDAO.findAll();
        all.remove(user);
        return all;

    }

    /**
     * Поиск пользователей по полю search исключая данного пользователя и пользователей находящихся с списке контактов
     * @param login логин пользователя
     * @param searchLogin часть логина по которому ведется поиск
     * @return
     */
    public List<User> searchUser(String login,String searchLogin){
        User user = userDAO.findByLogin(login);
        List<User> userList = userDAO.searchByLogin(searchLogin);
        List<Contact> contactList = user.getContactList();
        contactList.stream().forEach(contact -> userList.remove(contact.getContactUser()));
        userList.remove(user);
        return userList;
    }

    /**
     * Получение всех пользователей
     * @return
     */
    public List<User> listUser(){
        return userDAO.findAll();
    }

    /**
     * Выборка пользователя по логину
     * @param login
     * @return
     */
    public User findUser(String login){
        return userDAO.findByLogin(login);
    }

    /**
     * Обновление объекта {@User}
     * @param user
     */
    @Transactional
    public void updateUser(User user){
        userDAO.merge(user);
    }


    /**
     * Выборка пользователя по ИД
     * @param id
     * @return
     */
    public User findUserById(Long id){
       return userDAO.find(id);
    }


    /**
     * Метод шифрования пароля
     * @param password пароль для шифрования
     * @return пароль в защифрованном виде
     */
    public String encoder(String password){
        return passwordEncoder.encode(password);
    }

    /**
     * Есть ли пользователь с данным логином
     * @param login логин пользователя
     * @return
     */
    public boolean isLogin(String login){
        return userDAO.isLogin(login);
    }

    /**
     * Удаление пользователя
     * @param id ид пользователя
     */
    @Transactional
    public void remove(Long id){
        User user = userDAO.find(id);
        userDAO.remove(user);
    }


}
