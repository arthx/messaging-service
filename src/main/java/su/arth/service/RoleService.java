package su.arth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import su.arth.dao.RoleDAO;
import su.arth.entity.Role;

/**
 * Класс является сервисом Spring
 * управляет данными (Role)
 *
 */
@Service
public class RoleService {

    /**
     * Инжектит {@link RoleDAO}
     */
    @Autowired
    private RoleDAO roleDAO;

    /**
     * поиск объекта Role из базы данных по ид
     * @param idRole ИД роли
     * @return роль
     */
    public Role findById(Long idRole){
        return roleDAO.find(idRole);
    }



    /**
     * поиск объекта Role из базы данных по типу роли
     * @param roleType типа роли
     * @return роль
     */
    public Role findRole(String roleType){
        return roleDAO.findRole(roleType);
    }

}
