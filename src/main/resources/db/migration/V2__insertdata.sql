INSERT INTO role (rolename, roletype) VALUES ('Администратор', 'ROLE_ADMIN');
INSERT INTO role (rolename, roletype) VALUES ('Пользователь', 'ROLE_USER');


INSERT INTO user (login, password, fio, email) VALUES ('arth', '$2a$10$9MVxxtNtDpWa1hJN2RN89ueF9meuqDzaslJxNm..B8x/PGWPIJW.a', 'Arthur', 'arthurik97@gmail.com');
INSERT INTO user (login, password, fio, email) VALUES ('admin', '$2a$10$REDb14XKKy8.emSvZBjB9uhuomheaGjhmuZ/sucA3a5mmc/LOqi02', 'admin', 'admin@admin.ru');
INSERT INTO user (login, password, fio, email) VALUES ('user', '$2a$10$ZKh5Gx3vCzYcv1Sxr1YXX.5s40vSKwblMyXXz8JS9uJFkGdqW.fQm', 'user', 'user@user.ru');
INSERT INTO user_role (user_id, role_id) VALUES (1, 2);
INSERT INTO user_role (user_id, role_id) VALUES (2, 2);
INSERT INTO user_role (user_id, role_id) VALUES (3, 2);
INSERT INTO user_role (user_id, role_id) VALUES (1, 1);
INSERT INTO user_role (user_id, role_id) VALUES (2, 1);