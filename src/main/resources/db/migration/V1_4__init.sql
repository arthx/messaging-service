

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(45) NULL DEFAULT NULL,
  `password` VARCHAR(255) NULL DEFAULT NULL,
  `fio` VARCHAR(100) NULL DEFAULT NULL,
  `email` VARCHAR(35) NULL DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE INDEX `login_UNIQUE` (`login` ASC),
  UNIQUE INDEX `user_email_uindex` (`email` ASC))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `message` (
  `id_message` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `theme` VARCHAR(45) NOT NULL,
  `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `recipient` BIGINT(20) NULL DEFAULT NULL,
  `sender` BIGINT(20) NOT NULL,
  `content_message` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id_message`),
  FOREIGN KEY (`recipient`)
  REFERENCES `user` (`id_user`),
  FOREIGN KEY (`sender`)
  REFERENCES `user` (`id_user`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;


CREATE TABLE IF NOT EXISTS `role` (
  `id_role` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `rolename` VARCHAR(45) NULL DEFAULT NULL,
  `roletype` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id_role`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;


CREATE TABLE IF NOT EXISTS `contacts` (
  `id_contact` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `name_contact` VARCHAR(45) NULL DEFAULT NULL,
  `owner` BIGINT(20) NOT NULL,
  `contact_user` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id_contact`),
  INDEX `fk_contacts_user1_idx` (`owner` ASC),
  INDEX `fk_contacts_user2_idx` (`contact_user` ASC),
  FOREIGN KEY (`owner`)
  REFERENCES `user` (`id_user`),
  FOREIGN KEY (`contact_user`)
  REFERENCES `user` (`id_user`),
  CONSTRAINT `fk_contacts_user1`
  FOREIGN KEY (`owner`)
  REFERENCES `user` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contacts_user2`
  FOREIGN KEY (`contact_user`)
  REFERENCES `user` (`id_user`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;





CREATE TABLE IF NOT EXISTS `user_role` (
  `user_id` BIGINT(20) NULL DEFAULT NULL,
  `role_id` BIGINT(20) NULL DEFAULT NULL,
  INDEX `fk_user_role_user_idx` (`user_id` ASC),
  INDEX `fk_user_role_role1_idx` (`role_id` ASC),
  FOREIGN KEY (`user_id`)
  REFERENCES `user` (`id_user`),
  FOREIGN KEY (`role_id`)
  REFERENCES `role` (`id_role`),
  FOREIGN KEY (`role_id`)
  REFERENCES `role` (`id_role`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_role_user`
  FOREIGN KEY (`user_id`)
  REFERENCES `user` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;
