-- магазины
CREATE TABLE Shop
(
Id      INT,
Name    VARCHAR(255)
);

-- Товары
CREATE TABLE Good
(
Id   INT,
Name VARCHAR(255)
);

-- Товары в магазинах
CREATE TABLE ShopData
(
Id INT,
GoodId   INT,
ShopId    INT,
Amount   NUMERIC(18)
);