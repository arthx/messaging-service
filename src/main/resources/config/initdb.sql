-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema messagingdb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema messagingdb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `messagingdb` DEFAULT CHARACTER SET utf8 ;
USE `messagingdb` ;

-- -----------------------------------------------------
-- Table `messagingdb`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `messagingdb`.`user` (
  `id_user` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(45) NULL DEFAULT NULL,
  `password` VARCHAR(255) NULL DEFAULT NULL,
  `fio` VARCHAR(100) NULL DEFAULT NULL,
  `email` VARCHAR(35) NULL DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE INDEX `login_UNIQUE` (`login` ASC),
  UNIQUE INDEX `user_email_uindex` (`email` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 45
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `messagingdb`.`contacts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `messagingdb`.`contacts` (
  `id_contact` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `name_contact` VARCHAR(45) NULL DEFAULT NULL,
  `owner` BIGINT(20) NOT NULL,
  `contact_user` BIGINT(20) NOT NULL,
  PRIMARY KEY (`id_contact`),
  INDEX `fk_contacts_user1_idx` (`owner` ASC),
  INDEX `fk_contacts_user2_idx` (`contact_user` ASC),
  CONSTRAINT `FKgb6d1saxp3iu1owf1hxl458sy`
    FOREIGN KEY (`owner`)
    REFERENCES `messagingdb`.`user` (`id_user`),
  CONSTRAINT `FKnyc4xw7susnioyxdfwd77uxl6`
    FOREIGN KEY (`contact_user`)
    REFERENCES `messagingdb`.`user` (`id_user`),
  CONSTRAINT `fk_contacts_user1`
    FOREIGN KEY (`owner`)
    REFERENCES `messagingdb`.`user` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contacts_user2`
    FOREIGN KEY (`contact_user`)
    REFERENCES `messagingdb`.`user` (`id_user`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 33
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `messagingdb`.`message`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `messagingdb`.`message` (
  `id_message` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `theme` VARCHAR(45) NOT NULL,
  `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `recipient` BIGINT(20) NULL DEFAULT NULL,
  `sender` BIGINT(20) NOT NULL,
  `content_message` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id_message`),
  INDEX `FKdo0s9e4y2e2oj0uypi34pk0sc` (`recipient` ASC),
  INDEX `FKob83vkf2oo4r68pn9d69kgwf8` (`sender` ASC),
  CONSTRAINT `FKdo0s9e4y2e2oj0uypi34pk0sc`
    FOREIGN KEY (`recipient`)
    REFERENCES `messagingdb`.`user` (`id_user`),
  CONSTRAINT `FKob83vkf2oo4r68pn9d69kgwf8`
    FOREIGN KEY (`sender`)
    REFERENCES `messagingdb`.`user` (`id_user`))
ENGINE = InnoDB
AUTO_INCREMENT = 56
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `messagingdb`.`role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `messagingdb`.`role` (
  `id_role` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `rolename` VARCHAR(45) NULL DEFAULT NULL,
  `roletype` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id_role`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `messagingdb`.`schema_version`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `messagingdb`.`schema_version` (
  `installed_rank` INT(11) NOT NULL,
  `version` VARCHAR(50) NULL DEFAULT NULL,
  `description` VARCHAR(200) NOT NULL,
  `type` VARCHAR(20) NOT NULL,
  `script` VARCHAR(1000) NOT NULL,
  `checksum` INT(11) NULL DEFAULT NULL,
  `installed_by` VARCHAR(100) NOT NULL,
  `installed_on` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` INT(11) NOT NULL,
  `success` TINYINT(1) NOT NULL,
  PRIMARY KEY (`installed_rank`),
  INDEX `schema_version_s_idx` (`success` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `messagingdb`.`user_role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `messagingdb`.`user_role` (
  `user_id` BIGINT(20) NULL DEFAULT NULL,
  `role_id` BIGINT(20) NULL DEFAULT NULL,
  INDEX `fk_user_role_user_idx` (`user_id` ASC),
  INDEX `fk_user_role_role1_idx` (`role_id` ASC),
  CONSTRAINT `FK859n2jvi8ivhui0rl0esws6o`
    FOREIGN KEY (`user_id`)
    REFERENCES `messagingdb`.`user` (`id_user`),
  CONSTRAINT `FKa68196081fvovjhkek5m97n3y`
    FOREIGN KEY (`role_id`)
    REFERENCES `messagingdb`.`role` (`id_role`),
  CONSTRAINT `fk_user_role_role1`
    FOREIGN KEY (`role_id`)
    REFERENCES `messagingdb`.`role` (`id_role`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_role_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `messagingdb`.`user` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
