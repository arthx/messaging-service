/**
 * Created by arthur on 15.08.16.
 */
showAlert = function (element_id,status,message) {
    $("#"+element_id).html("<div class=\"alert alert-"+status+" alert-dismissible\" role=\"alert\">  "+
        "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">"+
        "<span aria-hidden=\"true\">&times;</span></button><strong>"+message+"</strong></div>");
    $("#"+element_id).fadeTo(2000, 500).slideUp(500, function(){
        $("#"+element_id).slideUp(500);
    });

}


sendNewMessage = function () {
    var msg   = $('#form-new-message').serialize();
    console.log(msg);
    $.post({
        url: '/message/new',
        beforeSend: function(xhr) { xhr.setRequestHeader('X-CSRF-TOKEN', $("meta[name='_csrf']").attr('content'))},
        data: msg,
        success: function(data) {
            showAlert("alert",data.statusName,data.message);
            $("#tab1").tab('show');
            $("#theme").val("");
            $("#content_message").val("");
        },
        error:  function(xhr, str){
            showAlert("alert","danger","Произошла ошибка при отправке сообщения");
        }
    });
}