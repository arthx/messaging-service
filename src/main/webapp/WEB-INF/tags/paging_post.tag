<%@ tag import="org.springframework.util.StringUtils" %>
<%@ tag pageEncoding="UTF-8" language="java" %>
<%@ attribute name="pagedListHolder" required="true" type="org.springframework.beans.support.PagedListHolder" %>
<%@ attribute name="pagedLink" required="true" type="java.lang.String" %>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"  %>
<link rel="stylesheet" href="/webjars/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="/webjars/jquery/2.2.4/jquery.min.js"></script>
<script src="/webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<c:if test="${pagedListHolder.pageCount>1}">
  <ul class="pagination">
    <c:if test="${!pagedListHolder.firstPage}">
      <li class="previous">
        <a href="<%=StringUtils.replace(pagedLink,"~",String.valueOf(pagedListHolder.getPage()-1))%>"><</a>
      </li>
    </c:if>

    <c:if test="${pagedListHolder.firstLinkedPage>0}">
      <li> <a href="<%=StringUtils.replace(pagedLink,"~","0")%>">1</a> </li>
    </c:if>
    <c:if test="${pagedListHolder.firstLinkedPage>1}" >
      <li> <span class="pagingDots">...</span>  </li>
    </c:if>
    <c:forEach begin="${pagedListHolder.firstLinkedPage}" end="${pagedListHolder.lastLinkedPage}" var="i">
      <c:choose>
        <c:when test="${pagedListHolder.page==i}">
          <li class="active"><a href="#">${i+1}</a> </li>
        </c:when>
        <c:otherwise>
          <li>
            <a href="<%=StringUtils.replace(pagedLink,"~", String.valueOf(jspContext.getAttribute("i")))%>">${i+1}</a>
          </li>
        </c:otherwise>
      </c:choose>
    </c:forEach>

    <c:if test="${pagedListHolder.lastLinkedPage < pagedListHolder.pageCount-2}">
      <li><span class="pagingDots">...</span> </li>
    </c:if>

    <c:if test="${pagedListHolder.lastLinkedPage < pagedListHolder.pageCount -1}">
      <li>
        <a href="<%=StringUtils.replace(pagedLink,"~",String.valueOf(pagedListHolder.getPageCount()-1))%>"> ${pagedListHolder.pageCount}</a>
      </li>
    </c:if>
    <c:if test="${!pagedListHolder.lastPage}">
      <li class="next">
        <a href="javascript:next()">></a>
      </li>
    </c:if>
  </ul>
</c:if>
<script type="text/javascript">
  function page(){
    $.post("/admin/users", { p: "<%= String.valueOf(pagedListHolder.getPage()+1)%>" } );
  }
  function next(){
    $.post("/admin/users", { p: "<%= String.valueOf(pagedListHolder.getPage()+1)%>" } );
  }
</script>