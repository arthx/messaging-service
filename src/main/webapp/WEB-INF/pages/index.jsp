<%--
  Created by IntelliJ IDEA.
  User: arthur
  Date: 08.08.16
  Time: 15:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tg" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="head.jsp"/>
<div id="container" class="container">
    <div id="alert"></div>
<div class="tabbable tabs-left">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab1"  data-toggle="tab"><i class="fa fa-inbox fa-lg"  aria-hidden="true"></i> Входящие сообщения</a></li>
        <li><a href="#tab2" data-toggle="tab"><i class="fa fa-inbox fa-lg"  aria-hidden="true"></i> Исходящие сообщения</a></li>
        <sec:authorize access="hasRole('ROLE_ADMIN')">  <li><a href="#tab4" data-toggle="tab"><i class="fa fa-bars fa-lg" aria-hidden="true"></i> Все сообщения</a></li></sec:authorize>
        <li><a href="#tab3" data-toggle="tab"><i class="fa fa-envelope-o fa-lg" aria-hidden="true"></i> Новое сообщение</a> </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
                <div id="page-selection-input" class="pagination">
               </div>
      <table id="input_message_table" class="table table-striped table-hover">
                <tbody>
                <tr> <th>От кого</th><th>Дата</th> <th>Тема</th> </tr>
                <c:forEach items="${inputMessageList.pageList}" var="message" >
                    <tr id="message_${message.idMessage}" class="message_click_input">
                        <td>${message.sender.login}</td>
                        <td>${message.date}</td>
                        <td>${message.theme}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="tab-pane" id="tab2">
            <div id="page-selection-output" class="pagination">
            </div>
            <table id="output_message_table" class="table table-striped table-hover">
                <tbody>
                <tr> <th>Кому</th><th>Дата</th> <th>Тема</th> </tr>
                <c:forEach items="${outputMessageList.pageList}" var="message">
                    <tr id="message_${message.idMessage}" class="message_click_output">
                        <td>${message.recipient.login}</td>
                        <td>${message.date}</td>
                        <td>${message.theme}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
                    </div>
        <div class="tab-pane" id="tab3">
            <form id="form-new-message" action="/message/new" method="post" >
                <fieldset>
                    <div class="input-group input-sm">
                        <label class="input-group-addon" for="recipient"><i class="fa fa-user fa-fw"></i></label>
                        <select id="recipient" class="form-control" name="recipient" required="required"></select>
                    </div>
                    <div class="input-group input-sm">
                        <label for="theme" class="input-group-addon" ><i class="fa fa-envelope fa-fw" aria-hidden="true"></i></label>
                        <input id="theme" class="form-control" name="theme" required="required" type="text" placeholder="Тема сообщения" />
                    </div>
                    <div class="input-group input-sm">
                        <label for="content_message" class="input-group-addon"><i class="fa fa-user fa-fw" aria-hidden="true"></i></label>
                        <textarea class="form-control" rows="9" required="required" id="content_message" name="content_message" placeholder="Сообщение"></textarea>
                    </div>
                    <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />
                    <div class="input-group input-sm">
                        <input type="submit" class="btn btn-primary btn-default" value="Отправить">
                    </div>
                </fieldset>
            </form>
        </div>


<sec:authorize access="hasRole('ROLE_ADMIN')">
        <div class="tab-pane" id="tab4">
            <div id="page-selection-all" class="pagination">
            </div>
            <table id="all_message_table" class="table table-striped table-hover">
                <tbody>
                <tr> <th>От кого</th><th>Дата</th> <th>Тема</th> </tr>
                <c:forEach items="${allMessageList.pageList}" var="message" >
                    <tr id="message_${message.idMessage}" class="message_click_all">
                        <td>${message.sender.login}</td>
                        <td>${message.date}</td>
                        <td>${message.theme}</td>
                        <td>${message.recipient.login}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
</sec:authorize>


    </div>
</div>

    <div id="open_message_window" class="modal fade ">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header"><button class="close" type="button" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Сообщение</h4>
                </div>
                <div id="open_message_content" class="modal-body">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" type="button" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
</div>
</div>

<script type="text/javascript" >


    $(document).ready(function () {
        <c:if test="${not empty status}">
        showAlert("alert","${status}","${message}");
        </c:if>
        load_select();
        message_input_click();
        message_output_click();
<sec:authorize access="hasRole('ROLE_ADMIN')">
        message_all_click();
        </sec:authorize>

     $("#open_message_window").modal('hide');

    });



 load_select = function () {
     $.get({
         url:'/contacts/my',
         success: function(data){
             var $recipient = $('#recipient');
             $recipient.empty();
             for (var i = 0; i < data.length; i++) {
                 $recipient.append('<option id=' + data[i].contactUser.idUser + ' value=' + data[i].contactUser.idUser + '>' + data[i].nameContact + '</option>');
             }
         }
     });
 }


 message_input_click = function () {
       $(".message_click_input").click(function () {
           var id_message = this.id.split('_')[1];

           $.get({
               url: "/message/" + id_message,
               success: function (data) {
                   $("#open_message_content").html("<div class=\"input-group input-sm\"><label class=\"input-group-addon\" for=\"theme\"><i class=\"fa fa-user\"></i></label><input type=\"text\" class=\"form-control\" id=\"theme\" name=\"theme\" value=\"" + data.theme + "\" readonly=\"true\">  </div>" +
                           "<div class=\"input-group input-sm\"><label class=\"input-group-addon\" for=\"sender\"><i class=\"fa fa-user\"></i></label><input type=\"text\" class=\"form-control\" id=\"sender\" name=\"sender\" value=\"" + data.sender.login + "\" readonly=\"true\"> " +
                           " <label class=\"input-group-addon\" for=\"date\"><i class=\"fa fa-user\"></i></label><input type=\"text\" class=\"form-control\" id=\"date\" name=\"date\" value=\"" + data.date + "\" readonly=\"true\"> </div>" +
                           "<div class=\"input-group input-sm\"><label class=\"input-group-addon\" for=\"content_message\"><i class=\"fa fa-list\" aria-hidden=\"true\"></i></label><textarea class=\"form-control\" id=\"content_message\" name=\"content_message\" rows=\"4\" readonly=\"true\">" + data.contentMessage + "</textarea></div>")
                   $("#open_message_window").modal('show');
                               }
           })


       })
   }


    message_output_click = function () {
        $(".message_click_output").click(function () {
            var id_message = this.id.split('_')[1];

            $.get({
                url: "/message/" + id_message,
                success: function (data) {
                    $("#open_message_content").html("<div class=\"input-group input-sm\"><label class=\"input-group-addon\" for=\"theme\"><i class=\"fa fa-user\"></i></label><input type=\"text\" class=\"form-control\" id=\"theme\" name=\"theme\" value=\"" + data.theme + "\" readonly=\"true\">  </div>" +
                            "<div class=\"input-group input-sm\"><label class=\"input-group-addon\" for=\"recipient\"><i class=\"fa fa-user\"></i></label><input type=\"text\" class=\"form-control\" id=\"recipient\" name=\"recipient\" value=\"" + data.recipient.login + "\" readonly=\"true\"> " +
                            " <label class=\"input-group-addon\" for=\"date\"><i class=\"fa fa-user\"></i></label><input type=\"text\" class=\"form-control\" id=\"date\" name=\"date\" value=\"" + data.date + "\" readonly=\"true\"> </div>" +
                            "<div class=\"input-group input-sm\"><label class=\"input-group-addon\" for=\"content_message\"><i class=\"fa fa-list\" aria-hidden=\"true\"></i></label><textarea class=\"form-control\" id=\"content_message\" name=\"content_message\" rows=\"4\" readonly=\"true\">" + data.contentMessage + "</textarea></div>")
                    $("#open_message_window").modal('show');
                }
            })


        })
    }


    $('#page-selection-input').bootpag({
        total: ${inputMessageList.pageCount},
        page: 1,
       // maxVisible: 5,
        leaps: true,
        firstLastUse: true,
        first: '←',
        last: '→',
        wrapClass: 'pagination',
        activeClass: 'active',
        disabledClass: 'disabled',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first'
    }).on("page", function(event, num){
        var page = num-1;
        $.get({
            url:"/message/input?p="+page,
            success: function(data){
                $("#input_message_table").html($(data).find("#input_message_table"));
            message_input_click();
            }
        })

    });

    $('#page-selection-output').bootpag({
        total: ${outputMessageList.pageCount},
        page: 1,
        // maxVisible: 5,
        leaps: true,
        firstLastUse: true,
        first: '←',
        last: '→',
        wrapClass: 'pagination',
        activeClass: 'active',
        disabledClass: 'disabled',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first'
    }).on("page", function(event, num){
        var page = num-1;
        $.get({
            url:"/message/output?p="+page,
            success: function(data){
                $("#output_message_table").html($(data).find("#output_message_table"));
                message_output_click();
            }
        })
    });

<sec:authorize access="hasRole('ROLE_ADMIN')">

    $('#page-selection-all').bootpag({
        total: ${allMessageList.pageCount},
        page: 1,
        leaps: true,
        firstLastUse: true,
        first: '←',
        last: '→',
        wrapClass: 'pagination',
        activeClass: 'active',
        disabledClass: 'disabled',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first'
    }).on("page", function(event, num){
        var page = num-1;
        $.get({
            url:"/message/all?p="+page,
            success: function(data){
                $("#all_message_table").html($(data).find("#all_message_table"));
                message_output_click();
            }
        })
    });


    message_all_click = function () {
        $(".message_click_all").click(function () {
            var id_message = this.id.split('_')[1];

            $.get({
                url: "/message/" + id_message,
                success: function (data) {
                    $("#open_message_content").html("<div class=\"input-group input-sm\"><label class=\"input-group-addon\" for=\"theme\"><i class=\"fa fa-user\"></i></label><input type=\"text\" class=\"form-control\" id=\"theme\" name=\"theme\" value=\"" + data.theme + "\" readonly=\"true\">  </div>" +
                            "<div class=\"input-group input-sm\"><label class=\"input-group-addon\" for=\"recipient\"><i class=\"fa fa-user\"></i></label><input type=\"text\" class=\"form-control\" id=\"recipient\" name=\"recipient\" value=\"" + data.recipient.login + "\" readonly=\"true\"> " +
                            " <label class=\"input-group-addon\" for=\"date\"><i class=\"fa fa-user\"></i></label><input type=\"text\" class=\"form-control\" id=\"date\" name=\"date\" value=\"" + data.date + "\" readonly=\"true\"> </div>" +
                            "<div class=\"input-group input-sm\"><label class=\"input-group-addon\" for=\"content_message\"><i class=\"fa fa-list\" aria-hidden=\"true\"></i></label><textarea class=\"form-control\" id=\"content_message\" name=\"content_message\" rows=\"4\" readonly=\"true\">" + data.contentMessage + "</textarea></div>")
                    $("#open_message_window").modal('show');
                }
            })


        })
    }





</sec:authorize>



</script>
</body>