<%--
  Created by IntelliJ IDEA.
  User: arthur
  Date: 08.08.16
  Time: 13:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%request.setCharacterEncoding("UTF-8");%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"  %>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="/webjars/bootstrap-switch/3.3.2/css/bootstrap3/bootstrap-switch.min.css">
   <link rel='stylesheet' href='webjars/bootstrap/3.3.6/css/bootstrap.min.css'>
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css" />
    <link href="/assets/css/style.css" rel="stylesheet" />
    <script src="/webjars/jquery/2.2.4/jquery.min.js"></script>
    <script src="/webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <script src="/webjars/bootstrap-switch/3.3.2/js/bootstrap-switch.min.js"></script>

</head>
<body>


<div id="mainWrapper">
    <c:if test="${not empty msg}">
        <div class="alert alert-${css} alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>${msg}</strong>
        </div>
    </c:if>

        <div class="login-container">
            <div class="login-card">
                <div class="login-form">
                    <sf:form method="POST" modelAttribute="user"  >
                        <fieldset>
                        <div class="input-group input-sm">
                            <label class="input-group-addon" for="login"><i class="fa fa-user fa-fw"></i></label>
                            <input type="text" class="form-control" id="login" name="login" placeholder="Введите логин" required>
                        </div>
                        <div class="input-group input-sm">
                            <label class="input-group-addon" for="password"><i class="fa fa-lock fa-fw"></i></label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Введите пароль" required>
                        </div>
                        <div class="input-group input-sm">
                            <label for="passwordsignup" class="input-group-addon"><i class="fa fa-lock fa-fw"></i></label>
                            <input id="passwordsignup" class="form-control" required="required" type="password" placeholder="Повторите пароль"/>
                        </div>
                            <div class="input-group input-sm">
                            <label for="email" class="input-group-addon" ><i class="fa fa-envelope fa-fw" aria-hidden="true"></i></label>
                            <input id="email" class="form-control" name="email" required="required" type="email" placeholder="test@test.ru" value="${user.email}"/>
                        </div>
                        <div class="input-group input-sm">
                            <label for="fio" class="input-group-addon"><i class="fa fa-user fa-fw" aria-hidden="true"></i></label>
                            <input type="text" class="form-control" required="required" id="fio" name="fio" placeholder="ФИО" value="${user.fio}"/>
                        </div>
                        <div class="input-group input-sm">
                        </div>
                            <sec:authorize access="hasRole('ROLE_ADMIN')">
                                <div class="input-group input-sm">
                                    <div class="make-switch">
                                        <div class="input-lg">
                                            <label for="role" >Администратор</label>
                                        </div>
                                        <sf:checkbox path="roles" value="1" id="role"/>
                                    </div>
                                </div>
                            </sec:authorize>

                        <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />

                        <div class="form-actions">
                            <p><sec:authorize access="hasRole('ROLE_ADMIN')"><a href="/admin/users" class="btn btn-primary btn-default signinbtn">Назад</a><input type="submit" class="btn btn-primary btn-default signinbtn" value="Добавить"></sec:authorize>
                                <sec:authorize access="hasRole('ROLE_ANONYMOUS')" > <input type="submit" class="btn btn-primary btn-default signinbtn" value="Я с вами"><a href="/signin" class="btn btn-primary signupbtn btn-default">А может все таки войти?</a></sec:authorize>
                        </div>
                        </fieldset>
                   </sf:form>
                </div>
    </div>
</div>
</div>
<script type="text/javascript">
     $("[name='roles']").bootstrapSwitch();
</script>
</body>