<%--
  Created by IntelliJ IDEA.
  User: arthur
  Date: 08.08.16
  Time: 13:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"  language="java" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>



<jsp:include page="../head.jsp"/>
<div class="container">
    <div id="alert"></div>
        <div class="input-group input-sm">
                <label for="login" class="input-group-addon" ><i class="fa fa-user fa-fw" aria-hidden="true"></i></label>
                <input id="login" class="form-control" name="login" type="text" readonly="readonly" value="${user.login}"/>
            </div>
            <div class="input-group input-sm">
                <label for="fio" class="input-group-addon"><i class="fa fa-user fa-fw" aria-hidden="true"></i></label>
                <input class="form-control" id="fio" name="fio" readonly="readonly" value="${user.fio}"/>
            </div>
            <div class="input-group input-sm">
                <button id="btn_change" class="btn btn-default btn-sm" type="button" data-toggle="modal" data-target="#change_password_window" ><i class="fa fa-cog"></i> Изменить пароль</button>
            </div>
        </fieldset>
    </form>

    <div id="change_password_window" class="modal fade ">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header"><button class="close" type="button" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Изменение пароля</h4>
                </div>
                <div id="open_message_content" class="modal-body">
                    <div id="alert_window"></div>
                    <form id="form-new-password" action="javascript:void(null);" onsubmit="changeRole()" method="post" >
                        <fieldset>
                            <div class="input-group input-sm">
                                <label for="password" class="input-group-addon" ><i class="fa fa-lock fa-fw" aria-hidden="true"></i></label>
                                <input id="password" class="form-control" name="password" required="required" type="password" placeholder="Введите пароль" />
                            </div>
                            <div class="input-group input-sm">
                                <label for="newPassword" class="input-group-addon" ><i class="fa fa-lock fa-fw" aria-hidden="true"></i></label>
                                <input id="newPassword" class="form-control" name="newPassword" required="required" type="password" placeholder="Введите новый пароль" />
                            </div>
                            <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />
                            <div class="input-group input-sm">
                                <input type="submit" class="btn btn-primary btn-default" value="Изменить пароль">
                            </div>
                        </fieldset>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" type="button" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
</div>

</boby>
<script type="text/javascript">

    $(document).ready(function () {

        <c:if test="${not empty status}">
         showAlert("alert","${status}","${message}")
        </c:if>

    });


    changeRole = function () {
        var msg   = $('#form-new-password').serialize();
        $("form")[0].reset();
        $.post({
            beforeSend: function(xhr) { xhr.setRequestHeader('X-CSRF-TOKEN', $("meta[name='_csrf']").attr('content'))},
            url: "/user/password/new",
            data: msg,
            timeout:10000,
            success: function (data) {
                if(data.status=='success'){

                    showAlert("alert",data.status,data.message);
                    $("#change_password_window").modal('hide');
                } else {
                    console.log("error");
                    showAlert("alert_window",data.status,data.message);

                }

            },
            error : function(e) {
               showAlert("alert_window","danger","Возникла ошибка");
            }
        })
    }

</script>