<%--
  Created by IntelliJ IDEA.
  User: arthur
  Date: 15.08.16
  Time: 17:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"  language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../head.jsp"/>

     <div id="container" class="container">
         <div id="alert"></div>
         <div class="tab-pane tabs-left">
             <ul class="nav nav-tabs">
                 <li class="active"><a href="#tab1"  data-toggle="tab"> <i class="fa fa-users fa-lg" aria-hidden="true"></i> Мои контакты</a></li>
                 <li><a href="#tab2" data-toggle="tab"><i class="fa fa-user fa-lg" aria-hidden="true"></i> Добавить новый контакт</a></li>
             </ul>
             <div class="tab-content">
                 <div class="tab-pane active" id="tab1">
                     <div id="page-selection" class="pagination">
                     </div>
                     <table id="contacts_table" class="table table-striped table-hover">
                         <tbody>
                         <tr> <th>Имя контакта</th><th>Логин</th> <th></th> </tr>
                         <c:forEach items="${contacts.pageList}" var="contact" >
                             <tr>
                                 <td>${contact.nameContact}</td>
                                 <td>${contact.contactUser.login}</td>
                                 <td> <a href="/contacts/remove/${contact.idContact}" class="btn btn-danger btn-sm"><i class="fa fa-trash-o fa-lg"></i> Удалить</a> </td>
                             </tr>
                         </c:forEach>
                         </tbody>
                     </table>


                 </div>
           <div class="tab-pane" id="tab2">
              <form role="form" id="form-new-contact" action="/contacts/new"  method="post" >
                 <fieldset>
                     <div class="form-group " >
                         <label for="contactUser" class="col-sm-4 control-label">Пользователь</label>
                         <div class="col-sm-8">
                             <select id="contactUser" class="js-data-user form-control" name="contactUser" style="width: 100%" ></select>
                         </div>
                     </div>
                  <div class="form-group">
                         <label class="col-sm-4 control-label">Имя контакта</label>
                         <div class="col-sm-8">
                            <input type="text" class="form-control" id="nameContact" name="nameContact" >
                         </div>
                     </div>
                     <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />
                     <div class="form-group">
                         <input type="submit" class="btn btn-primary btn-default" value="Сохранить">
                     </div>
                 </fieldset>
              </form>
                 </div>
             </div>
         </div>
     </div>


<script type="text/javascript">

    $(document).ready(function () {

        <c:if test="${not empty status}">
           showAlert("alert","${status}","${message}");
        </c:if>
       pagingFunction();


        $(".js-data-user").select2({
            language: "ru",
            ajax: {
                url: "/user/search",
                dataType: 'json',
                data: function (params) {
                    return {
                        search: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            minimumInputLength:1
        });
    });

pagingFunction = function () {
    $('#page-selection').bootpag({
        total: ${contacts.pageCount},
        page: 1,
        // maxVisible: 5,
        leaps: true,
        firstLastUse: true,
        first: '←',
        last: '→',
        wrapClass: 'pagination',
        activeClass: 'active',
        disabledClass: 'disabled',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first'
    }).on("page", function(event, num){
        var page = num-1;
        $.get({
            url:"/contacts?p="+page,
            success: function(data){
                $("#contacts_table").html($(data).find("#contacts_table"));
            }
        })

    });
}
</script>

</body>
</html>