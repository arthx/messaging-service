<%--
  Created by IntelliJ IDEA.
  User: arthur
  Date: 08.08.16
  Time: 13:15
  To change this template use File | Settings | File Templates.
--%>

<%@ include file="head.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="main" class="container" >

    <c:if test="${not empty msg}">
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>${msg}</strong>
        </div>
    </c:if>


</div>

</div>


