<%--
  Created by IntelliJ IDEA.
  User: arthur
  Date: 08.08.16
  Time: 13:14
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"  language="java" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="/assets/css/style.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css" />
    <link rel="stylesheet" href="/webjars/bootstrap/3.3.6/css/bootstrap.min.css" />
</head>
<body>
<div id="mainWrapper">
        <div class="login-container">
            <div class="login-card">
                             <div class="login-form">
                                 <c:if test="${not empty msg}">
                                     <div class="alert alert-${css} alert-dismissible" role="alert">
                                         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                             <span aria-hidden="true">&times;</span>
                                         </button>
                                         <strong>${msg}</strong>
                                     </div>
                                 </c:if>
                                 <sf:form method="POST" action="/j_spring_security_check" name="f" >
            <div class="input-group input-sm">
                <label class="input-group-addon" for="login"><i class="fa fa-user fa-fw"></i></label>
                <input type="text" class="form-control" id="login" name="login" placeholder="Введите логин" required>
            </div>
            <div class="input-group input-sm">
                <label class="input-group-addon" for="password"><i class="fa fa-lock fa-fw"></i></label>
                <input type="password" class="form-control" id="password" name="password" placeholder="Введите пароль" required>
            </div>
            <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />

            <div class="form-actions">
               <p> <input type="submit" class="btn btn-primary btn-default signinbtn" value="Войти"><a href="/user" class="btn btn-primary signupbtn btn-default">Регистрация</a>
            </div>
        </sf:form>
    </div>
</div>
</div></div></body></html>