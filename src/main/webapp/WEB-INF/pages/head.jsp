<%--
  Created by IntelliJ IDEA.
  User: arthur
  Date: 08.08.16
  Time: 12:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"  language="java" %>
<%request.setCharacterEncoding("UTF-8");%>
<%@taglib prefix="tg" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="_csrf" content="${_csrf.token}"/>
    <link href="/assets/css/index.css" rel="stylesheet"/>
    <link href="/assets/css/select2.min.css" rel="stylesheet"/>
    <link rel='stylesheet' href='/webjars/bootstrap/3.3.6/css/bootstrap.min.css'>
    <link rel="stylesheet" href="/webjars/bootstrap-switch/3.3.2/css/bootstrap3/bootstrap-switch.min.css">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css" />
    <script src="/webjars/jquery/2.2.4/jquery.min.js"></script>
    <script src="/webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="/webjars/bootstrap-switch/3.3.2/js/bootstrap-switch.min.js"></script>
    <script src="/assets/js/jquery.bootpag.min.js"></script>
    <script src="/assets/js/jquery.form.min.js"></script>
    <script src="/assets/js/select2.full.min.js"></script>
    <script src="/assets/js/select2/i18n/ru.js"></script>
    <script src="/assets/js/helper.js"></script>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Сообщения</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <%--<li class="active"><a href="/">Главная</a></li>--%>
                <li><a href="/contacts">Контакты</a></li>
                <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <li><a href="/admin/users">Пользователи</a></li>
                </sec:authorize>
                    <sec:authorize access="isAuthenticated()">

                <li ><a href="/user/mypage"> ${pageContext.request.userPrincipal.name}</a>
                </li>

                <li><a href="/logout">Выйти</a>
                </li>
                </sec:authorize>
                <sec:authorize access="!isAuthenticated()">
                    <li><a href="/signin">Войти</a></li>
                </sec:authorize>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>

