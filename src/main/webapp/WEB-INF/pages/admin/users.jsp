
<%--
  Created by IntelliJ IDEA.
  User: arthur
  Date: 08.08.16
  Time: 15:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%request.setCharacterEncoding("UTF-8");%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="tg" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<jsp:include page="../head.jsp"/>
<div class="container">
       <tg:paging pagedListHolder="${pagedListHolder}" pagedLink="/admin/users?p=~"/>
    <jsp:useBean id="pagedListHolder" scope="request" class="org.springframework.beans.support.PagedListHolder"/>
    <div class="alert"></div>
    <table class="table table-striped table-hover">
        <tbody>
        <tr> <th>ИД</th><th>ФИО</th> <th>Логин</th> <th>Почта</th> <th>  </th> <th></th>  </tr>
        <c:forEach items="${pagedListHolder.pageList}" var="user">
            <tr id="user_${user.idUser}">
                <td>${user.idUser}</td>
                <td>${user.fio}</td>
                <td> ${user.login}</td>
                <td>${user.email}</td>
                <td><button id="btn_${user.idUser}" onclick="openWindow(this.id)" class="btn btn-default btn-sm" type="button" data-toggle="modal" data-target="#myModal" ><i class="fa fa-cog"></i> Изменить</button>
                </td>
             <td><a href="/user/remove/${user.idUser}" class="btn btn-danger btn-sm"><i class="fa fa-trash-o fa-lg"></i> Удалить</a> </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
<div class="jumbotron center-block" >
    <a href="/user" class="btn btn-default btn-primary">Добавить пользователя</a>
</div>
      <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"><button class="close" type="button" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Изменить роль</h4>
                </div>
                <div class="modal-body">
                            <input type="hidden" id="idUser" />
                            <div class="input-group input-sm">
                                <label class="input-group-addon" for="login"><i class="fa fa-user"></i></label>
                                <input type="text" class="form-control" id="login" name="login" readonly>
                            </div>
                            <div class="input-group input-sm">
                                <div class="make-switch">
                                <input type="checkbox" id="role_admin" data-label-text="Администратор">
                             </div>
                            </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default" onclick="changeRole()" type="button" data-dismiss="modal">Сохранить</button>
                    <button class="btn btn-default" type="button" data-dismiss="modal">Закрыть</button>
                </div>
        </div>
      </div>

      </div>



</div>



<script src="/webjars/bootstrap-switch/3.3.2/js/bootstrap-switch.min.js"></script>

</body>
<script type="text/javascript" >
      $("#role_admin").bootstrapSwitch();
        openWindow = function (id) {
            id_user = id.substring(4);
            $("#idUser").val(id_user);
            $.get({
                beforeSend: function(xhr) { xhr.setRequestHeader('X-CSRF-TOKEN', $("meta[name='_csrf']").attr('content'))},
                url: "/admin/change/"+id_user,
                timeout:10000,
                success: function (data) {
                    $("#login").val(data["idUser"]);
                    $("#role_admin").bootstrapSwitch('state', JSON.parse(data["roleAdmin"]));
                },
                error : function(e) {
                    $("#alert").html("<div class=\"alert alert-danger alert-dismissible\" role=\"alert\">  "+
                            "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">"+
                            "<span aria-hidden=\"true\">&times;</span></button><strong>Произошла ошибка. Повторите операцию еще раз</strong></div>");
                    $("#alert").fadeTo(2000, 500).slideUp(500, function(){
                        $("#alert").slideUp(500);
                    });
                }
            });

        }

        changeRole = function () {
             var id_user = $("#idUser").val();
            data = {};
            data["idUser"] = id_user;
            data["roleAdmin"] = $("#role_admin").is(":checked");
            $.ajax({
                type: "POST",
                contentType: "application/json",
                beforeSend: function(xhr) { xhr.setRequestHeader('X-CSRF-TOKEN', $("meta[name='_csrf']").attr('content'))},
                url: "/admin/change",
                 data: JSON.stringify(data),
                dataType: "json",
                timeout:10000,
                success: function (data) {
                    $("#alert").html("<div class=\"alert alert-info alert-dismissible\" role=\"alert\">  "+
                            "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">"+
                            "<span aria-hidden=\"true\">&times;</span></button><strong>Операция прошла успешно</strong></div>");
                    $("#alert").fadeTo(2000, 500).slideUp(500, function(){
                        $("#alert").slideUp(500);
                    });
                },
                error : function(e) {
                    $("#alert").html("<div class=\"alert alert-danger alert-dismissible\" role=\"alert\">  "+
                            "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">"+
                            "<span aria-hidden=\"true\">&times;</span></button><strong>Произошла ошибка. Повторите операцию еще раз</strong></div>");
                    $("#alert").fadeTo(2000, 500).slideUp(500, function(){
                        $("#alert").slideUp(500);
                    });
                }
            })
        }

</script>
