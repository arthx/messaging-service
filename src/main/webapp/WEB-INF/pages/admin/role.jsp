<%--
  Created by IntelliJ IDEA.
  User: arthur
  Date: 10.08.16
  Time: 17:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"  language="java" %>

<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ include file="../head.jsp"%>


<div id="mainWrapper">
    <c:if test="${not empty msg}">
    <div class="alert alert-${css} alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>${msg}</strong>
    </div>
    </c:if>

    <div class="login-container">
        <div class="login-card">
            <div class="login-form">
                <sf:form method="POST" modelAttribute="user" action="/user"  >
                    <fieldset>
                        <div class="input-group input-sm">
                            <label class="input-group-addon" for="login"><i class="fa fa-user"></i></label>
                            <input type="text" class="form-control" id="login" name="login" value="login" required>
                        </div>


                            <div class="input-group input-sm">
                                <div class="make-switch">
                                    <label for="role">Администратор</label>
                                <sf:checkbox path="roles" value="1" id="role" label="Администратор" />
                                    </div>
                            </div>

                        <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />

                        <div class="form-actions">
                            <p><sec:authorize access="hasRole('ROLE_ADMIN')"><input type="submit" class="btn btn-primary btn-default signinbtn" value="Сохранить"></sec:authorize></p> </div>
                    </fieldset>
                </sf:form>
            </div>

        </div>

    </div>

</div>
</body>