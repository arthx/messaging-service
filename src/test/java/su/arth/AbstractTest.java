package su.arth;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import su.arth.config.WebAppConfig;

/**
 * Created by arthur on 04.09.16.
 */
@WebAppConfiguration
@ContextConfiguration(classes = WebAppConfig.class)
public class AbstractTest {

}
